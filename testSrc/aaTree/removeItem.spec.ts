import { expect } from "chai";
import { addItem } from "../../src/aaTree/addItem";
import { createAANode } from "../../src/aaTree/createAANode";
import { removeItem } from "../../src/aaTree/removeItem";
import { isTerminator, terminator } from "../../src/aaTree/terminator";
import { createUniqueKeys } from "../createKeys";
import { validateAaTreeConstraints } from "./validate/validateAaTreeConstraints";
import { validateNodeCount } from "./validate/validateNodeCount";
import { validateTreeDepth } from "./validate/validateTreeDepth";

describe("removeItem", function (): void {
  it("should remove items, and retain valid balanced tree structure", function (): void {
    // arrange
    const items = createUniqueKeys(200);
    const subSet = items.slice(50, 150);
    let tree = terminator;
    items.forEach(item => tree = addItem(tree, item, createAANode, null));

    // act
    subSet.forEach(item => tree = removeItem(tree, item));

    // assert
    validateNodeCount(tree, items.length - subSet.length);
    validateAaTreeConstraints(tree);
    validateTreeDepth(tree);
  });

  it("should return if trying to remove from empty tree", function (): void {
    // arrange

    let tree = terminator;

    // act
    tree = removeItem(tree, 1);

    // assert
    expect(isTerminator(tree)).equals(true);
  });
});
