import { addItem } from "../../src/aaTree/addItem";
import { createAANode } from "../../src/aaTree/createAANode";
import { terminator } from "../../src/aaTree/terminator";
import { createUniqueKeys } from "../createKeys";
import { validateNodeCount } from "./validate/validateNodeCount";
import { validateAaTreeConstraints } from "./validate/validateAaTreeConstraints";
import { validateTreeDepth } from "./validate/validateTreeDepth";

describe("addItem", function (): void {
  it("should add items, and retain valid balanced tree structure", function (): void {
    // arrange
    const items = createUniqueKeys(500);
    let tree = terminator;

    // act
    items.forEach(item => tree = addItem(tree, item, createAANode, null));

    // assert
    validateNodeCount(tree, items.length);
    validateAaTreeConstraints(tree);
    validateTreeDepth(tree);
  });

  it("should work with large tree", function (): void {
    // arrange
    const items = createUniqueKeys(100000);
    let tree = terminator;
    // act
    items.forEach(item => tree = addItem(tree, item, createAANode, null));

    // assert
    // if test takes over 2 seconds, it will autofail, no need for assertions
    // it would mean the tree is unbalanced
  });
});
