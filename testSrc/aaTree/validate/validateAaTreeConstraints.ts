import { expect } from "chai";
import { AANode } from "../../../src/aaTree/aaNode.type";
import { isLeaf } from "../../../src/aaTree/isLeaf";
import { isTerminator } from "../../../src/aaTree/terminator";


// 1.) The level of leaf node is 1.
// 2.) The level of left child is exactly one less than of its parent.
// 3.) The level of every right child is is equal to or one less than of its parent.
// 4.) The level of every right grandchild is strictly less than that of its grandparent.
// 5.) Every node of level greater than one has two children.

export function validateAaTreeConstraints<TKey>(node: AANode<TKey>): void {
  if (isLeaf(node)) {
    expect(node.level).equals(1);
    return;
  }

  if (!isTerminator(node.left)) {
    expect(node.left.level).equals(node.level - 1);
    validateAaTreeConstraints(node.left);
  }

  if (!isTerminator(node.right)) {
    const red = node.level === node.right.level;
    const black = node.level - 1 === node.right.level;
    expect(red || black).equals(true);

    if (!isTerminator(node.right.right)) {
      expect(node.right.right.level < node.level).equals(true);
    }
    validateAaTreeConstraints(node.right);
  }

  if (node.level > 1) {
    expect(isTerminator(node.left)).equals(false);
    expect(isTerminator(node.right)).equals(false);
  }
}
