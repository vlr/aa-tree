import { expect } from "chai";
import { treeCount } from "./treeCount";
import { AANode } from "../../../src/aaTree/aaNode.type";

export function validateNodeCount<TKey>(node: AANode<TKey>, count: number): void {
  expect(treeCount(node)).equals(count);
}
