import { AANode } from "../../../src/aaTree/aaNode.type";
import { isTerminator } from "../../../src/aaTree/terminator";

export function treeCount<TKey>(node: AANode<TKey>): number {
  return isTerminator(node) ? 0 : (1 + treeCount(node.left) + treeCount(node.right));
}
