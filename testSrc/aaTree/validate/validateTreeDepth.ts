import { treeCount } from "./treeCount";
import { expect } from "chai";
import { AANode } from "../../../src/aaTree/aaNode.type";
import { isTerminator } from "../../../src/aaTree/terminator";

export function validateTreeDepth<TKey>(node: AANode<TKey>): void {
  const count = treeCount(node);
  const maxDepth = treeDepth(node);

  // there can be maximum of log(N) red nodes.
  // red nodes can make half of tree depth
  const pow = Math.pow(2, Math.floor(maxDepth / 2));
  expect(pow <= count).equals(true);
}

function treeDepth<TKey>(node: AANode<TKey>): number {
  return isTerminator(node) ? 0 : (1 + Math.max(treeDepth(node.left), treeDepth(node.right)));
}
