import { terminator } from "../../src/aaTree/terminator";
import { addStackItem } from "../../src/stackTree/addStackItem";
import { createUniqueItems } from "../testType";
import { validateTreeWeight } from "./validateTreeWeight";
import { validateNodeCount } from "../aaTree/validate/validateNodeCount";
import { validateAaTreeConstraints } from "../aaTree/validate/validateAaTreeConstraints";
import { validateTreeDepth } from "../aaTree/validate/validateTreeDepth";

describe("addItem", function (): void {
  it("should add non unique items", function (): void {
    // arrange
    let items = createUniqueItems(500);
    items = [...items, ...items];
    let tree = terminator;

    // act
    items.forEach(item => tree = addStackItem(tree, item, item.key));

    // assert
    validateNodeCount(tree, items.length / 2);
    validateTreeWeight(tree, items.length);
    validateAaTreeConstraints(tree);
    validateTreeDepth(tree);
  });
});
