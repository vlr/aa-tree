import { expect } from "chai";
import { StackTree } from "../../src";
import { createTestItem } from "../testType";

describe("stackTree", function (): void {
  it("shift should remove last item and return null", function (): void {
    // arrange
    let items = [4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    tree.shift();
    const result2 = tree.shift();

    // assert
    expect(result2 == null).equals(true);
  });

  it("shiftGroup should remove last item", function (): void {
    // arrange
    let items = [4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    tree.shiftGroup();
    const result2 = tree.shiftGroup();

    // assert
    expect(result2 == null).equals(true);
  });

  it("pop should remove last item", function (): void {
    // arrange
    let items = [4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    tree.pop();
    const result2 = tree.pop();

    // assert
    expect(result2 == null).equals(true);
  });

  it("popGroup should remove last item", function (): void {
    // arrange
    let items = [4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    tree.popGroup();
    const result2 = tree.popGroup();

    // assert
    expect(result2 == null).equals(true);
  });


});
