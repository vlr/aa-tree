import { expect } from "chai";
import { StackTree } from "../../src";
import { createTestItem, TestType } from "../testType";

describe("stackTree", function (): void {
  it("should return first item", function (): void {
    // arrange
    let items = [4, 5, 2, 3, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.first();
    const result2 = tree.first();

    // assert
    expect(result).equals(items[2]);
    expect(result2).equals(items[2]);
  });

  it("first should return null off empty tree", function (): void {
    // arrange
    let tree = new StackTree((item: TestType) => item.key);

    // act
    const result = tree.first();

    // assert
    expect(result == null).equals(true);
  });

  it("should return last item", function (): void {
    // arrange
    let items = [4, 5, 2, 3, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.last();
    const result2 = tree.last();

    // assert
    expect(result).equals(items[4]);
    expect(result2).equals(items[4]);
  });

  it("last should return null off empty tree", function (): void {
    // arrange
    let tree = new StackTree((item: TestType) => item.key);

    // act
    const result = tree.last();

    // assert
    expect(result == null).equals(true);
  });


  it("should shift item", function (): void {
    // arrange
    let items = [4, 5, 2, 2, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.shift();
    const result2 = tree.shift();

    // assert
    expect(result).equals(items[2]);
    expect(result2).equals(items[3]);
  });

  it("should shift group of item", function (): void {
    // arrange
    let items = [4, 6, 2, 3, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.shiftGroup();

    // assert
    expect(result).deep.equals([items[2]]);
  });

  it("shift should return null off empty tree", function (): void {
    // arrange
    let tree = new StackTree((item: TestType) => item.key);

    // act
    const result = tree.shift();
    const result2 = tree.shiftGroup();

    // assert
    expect(result == null).equals(true);
    expect(result2 == null).equals(true);
  });

  it("should pop item", function (): void {
    // arrange
    let items = [4, 6, 2, 3, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.pop();
    const result2 = tree.pop();

    // assert
    expect(result).equals(items[4]);
    expect(result2).equals(items[1]);
  });

  it("should pop group of item", function (): void {
    // arrange
    let items = [4, 6, 2, 3, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.popGroup();

    // assert
    expect(result).deep.equals([items[1], items[4]]);
  });

  it("pop should return null off empty tree", function (): void {
    // arrange
    let tree = new StackTree((item: TestType) => item.key);

    // act
    const result = tree.pop();
    const result2 = tree.popGroup();

    // assert
    expect(result == null).equals(true);
    expect(result2 == null).equals(true);
  });

  it("should add item", function (): void {
    // arrange
    let items = [4, 5, 2].map(createTestItem);
    let tree = new StackTree(item => item.key, ...items);
    let newItem = createTestItem(1);

    // act
    tree.add(newItem, 1);
    const result = tree.first();

    // assert
    expect(result).equals(newItem);
  });

  it("isEmpty should work", function (): void {
    // arrange
    let tree = new StackTree(item => item.key, createTestItem(5));

    // act
    const empty1 = tree.isEmpty();
    tree.pop();
    const empty2 = tree.isEmpty();

    // assert
    expect(empty1).equals(false);
    expect(empty2).equals(true);
  });
});
