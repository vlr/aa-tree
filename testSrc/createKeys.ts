import { range, unique } from "@vlr/array-tools";

export function createKeys(amount: number): number[] {
  return range(amount).map(random);
}

export function createUniqueKeys(amount: number): number[] {
  return unique(createKeys(amount * 1.5)).slice(0, amount);
}

function random(): number {
  return Math.floor(Math.random() * 10000000);
}
