import { createKeys, createUniqueKeys } from "./createKeys";

export interface TestType {
  key: number;
  value: string;
}

let textNum = 0;

export function createTestItem(key: number): TestType {
  return {
    key,
    value: `item ${textNum++}`
  };
}

export function createItems(amount: number): TestType[] {
  return createKeys(amount).map(createTestItem);
}

export function createUniqueItems(amount: number): TestType[] {
  return createUniqueKeys(amount).map(createTestItem);
}
