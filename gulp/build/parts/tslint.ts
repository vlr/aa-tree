import { spawn } from "@vlr/spawn";

export function tslint(): Promise<void> {
  return spawn("tslint", "npx", "tslint", "-p", "tsconfig.json");
}
