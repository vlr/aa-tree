import { series } from "gulp";
import {
  createRepoOnGitLab, goOneDirUp, cloneSeedToNewLibDir, goIntoLibDir, installNodeModules,
  changeRemote, pushMaster, setRepoEnvVariables, setMasterProtection, createBranch,
  replaceRemoteInPackage, replaceRemoteInGitlabCi, pushChangesToBranch, ensureFilesExist, installRequest, gitReset, goBackToSeed
} from "./steps";

export const startNewLib = series(
  ensureFilesExist,
  installRequest,
  createRepoOnGitLab,
  setRepoEnvVariables,
  goOneDirUp,
  cloneSeedToNewLibDir,
  goIntoLibDir,
  installNodeModules,
  changeRemote,
  pushMaster,
  setMasterProtection,
  createBranch,
  replaceRemoteInPackage,
  replaceRemoteInGitlabCi,
  pushChangesToBranch,
  goBackToSeed,
  gitReset
);
