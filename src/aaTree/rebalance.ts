import { AANode } from "./aaNode.type";
import { skew } from "./skew";
import { split } from "./split";
import { isTerminator } from "./terminator";

export function rebalance<TKey, TNode extends AANode<TKey>>(node: TNode): TNode {
  if (node.level - node.left.level < 2 && node.level - node.right.level < 2) {
    return node;
  }

  node.level--;
  if (node.right.level > node.level) { node.right.level = node.level; }

  // three skews
  node = skew(node);
  node.right = skew(node.right);
  if (!isTerminator(node.right.right)) { node.right.right = skew(node.right.right); }

  // two splits
  node = split(node);
  node.right = split(node.right);
  return node;
}

