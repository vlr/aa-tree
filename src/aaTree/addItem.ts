import { AANode } from "./aaNode.type";
import { skew } from "./skew";
import { split } from "./split";
import { isTerminator } from "./terminator";

export type Add<TKey, TNode extends AANode<TKey>> = (key: TKey) => TNode;
export type Upd<TKey, TNode extends AANode<TKey>> = (node: TNode) => TNode;

export function addItem<TKey, TNode extends AANode<TKey>>(node: TNode, key: TKey, add: Add<TKey, TNode>, upd: Upd<TKey, TNode>): TNode {
  if (isTerminator(node)) { return add(key); }

  if (key < node.key) {
    node.left = addItem(node.left, key, add, upd);
    return split(skew(node));
  }

  if (key > node.key) {
    node.right = addItem(node.right, key, add, upd);
    return split(node);
  }

  // must be key is equal
  return upd(node);
}
