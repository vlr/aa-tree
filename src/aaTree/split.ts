import { AANode } from "./aaNode.type";
import { isTerminator } from "./terminator";

export function split<TKey, TNode extends AANode<TKey>>(node: TNode): TNode {
  // no need to split
  if (isTerminator(node.right) || node.right.right.level < node.level) {
    return node;
  }

  const right = node.right;
  node.right = right.left;
  right.left = node;
  right.level++;
  return <TNode>right;
}
