import { terminator } from "./terminator";
import { AANode } from "./aaNode.type";

export function createAANode<TKey>(key: TKey): AANode<TKey> {
  return {
    key,
    level: 1,
    right: terminator,
    left: terminator
  };
}
