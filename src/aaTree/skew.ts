import { AANode } from "./aaNode.type";

export function skew<TKey, TNode extends AANode<TKey>>(node: TNode): TNode {
  // no need to skew
  if (node.level > node.left.level) {
    return node;
  }

  const left = node.left;
  node.left = left.right;
  left.right = node;
  return <TNode>left;
}
