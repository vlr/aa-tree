import { AANode } from "./aaNode.type";
import { isTerminator } from "./terminator";

export function isLeaf<TKey>(node: AANode<TKey>): boolean {
  return isTerminator(node.left) && isTerminator(node.right);
}
