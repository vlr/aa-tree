import { AANode } from "./aaNode.type";
import { rebalance } from "./rebalance";
import { isTerminator } from "./terminator";

export function removeItem<TKey, TNode extends AANode<TKey>>(node: TNode, key: TKey): TNode {
  if (isTerminator(node)) { return node; }

  if (key < node.key) {
    node.left = removeItem(node.left, key);
    return rebalance(node);
  }

  if (key > node.key) {
    node.right = removeItem(node.right, key);
    return rebalance(node);
  }

  // must be key is equal
  if (isTerminator(node.left)) { return <TNode>node.right; }

  // internal node, has both children
  node = replaceWithPredecessor(node);
  node.left = removeItem(node.left, node.key);
  return rebalance(node);
}

function replaceWithPredecessor<TKey, TNode extends AANode<TKey>>(node: TNode): TNode {
  let predecessor = node.left;
  while (!isTerminator(predecessor.right)) { predecessor = predecessor.right; }
  return {
    ...<TNode>predecessor,
    level: node.level,
    left: node.left,
    right: node.right
  };
}


