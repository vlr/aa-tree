import { MultiValueNode } from "./multiValueNode.type";
import { isTerminator } from "../aaTree/terminator";

export function leftNode<T, TKey>(root: MultiValueNode<T, TKey>): MultiValueNode<T, TKey> {
  if (isTerminator(root)) { return root; }
  while (!isTerminator(root.left)) { root = root.left; }
  return root;
}

export function rightNode<T, TKey>(root: MultiValueNode<T, TKey>): MultiValueNode<T, TKey> {
  if (isTerminator(root)) { return root; }
  while (!isTerminator(root.right)) { root = root.right; }
  return root;
}
