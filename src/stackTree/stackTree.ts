import { first, last } from "@vlr/array-tools";
import { removeItem } from "../aaTree/removeItem";
import { isTerminator, terminator } from "../aaTree/terminator";
import { addStackItem } from "./addStackItem";
import { leftNode, rightNode } from "./leftNode";
import { MultiValueNode } from "./multiValueNode.type";

export class StackTree<T, TKey> {
  private root: MultiValueNode<T, TKey> = terminator;

  constructor(private selector: (i: T) => TKey, ...items: T[]) {
    this.addRange(items);
  }

  public add(item: T, key?: TKey): void {
    if (key == null) { key = this.selector(item); }
    this.root = addStackItem(this.root, item, key);
  }

  public pop(): T {
    const node = rightNode(this.root);
    if (isTerminator(node)) { return null; }
    const result = node.items.pop();
    if (node.items.length === 0) { this.remove(node.key); }
    return result;
  }

  public popGroup(): T[] {
    const node = rightNode(this.root);
    if (isTerminator(node)) { return null; }
    const result = node.items;
    this.remove(node.key);
    return result;
  }

  public last(): T {
    const node = rightNode(this.root);
    if (isTerminator(node)) { return null; }
    return last(node.items);
  }

  public shift(): T {
    const node = leftNode(this.root);
    if (isTerminator(node)) { return null; }
    const result = node.items.shift();
    if (node.items.length === 0) { this.remove(node.key); }
    return result;
  }

  public shiftGroup(): T[] {
    const node = leftNode(this.root);
    if (isTerminator(node)) { return null; }
    const result = node.items;
    this.remove(node.key);
    return result;
  }

  public first(): T {
    const node = leftNode(this.root);
    if (isTerminator(node)) { return null; }
    return first(node.items);
  }

  public addRange(items: T[]): void {
    items.forEach(item => this.add(item));
  }

  public isEmpty(): boolean {
    return isTerminator(this.root);
  }

  private remove(key: TKey): void {
    this.root = removeItem(this.root, key);
  }
}
