import { MultiValueNode, createMultiValueNode } from "./multiValueNode.type";
import { addItem } from "../aaTree/addItem";

export function addStackItem<T, TKey>(root: MultiValueNode<T, TKey>, item: T, key: TKey): MultiValueNode<T, TKey> {
  return addItem(root, key, () => createMultiValueNode(item, key), node => {
    node.items.push(item);
    return node;
  });
}
